﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance = null;

        public decimal Balance { get; set; }
        public int Capacity { get; set; }
        public List<Vehicle> Vehicles { get; set; }

        private Parking()
        {
            Clear();
        }

        public void Clear()
        {
            Balance = Settings.ParkingStartBalance;
            Capacity = Settings.ParkingCapacity;
            Vehicles = new List<Vehicle>();
        }

        public static Parking GetInstance()
        {
            if (instance == null)
                instance = new Parking();
            return instance;
        }
    }
}