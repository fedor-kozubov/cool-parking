﻿namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static int ParkingStartBalance { get; set; } = 0;
        public static int ParkingCapacity { get; set; } = 10;
        public static int WithdrawInterval { get; set; } = 5;
        public static int LogInterval { get; set; } = 60;
        public static decimal[] Tariffs { get; set; } = new decimal[] { 2.0M, 5.0M, 3.5M, 1.0M };
        public static decimal FineKoef { get; set; } = 2.5M;
    }
}