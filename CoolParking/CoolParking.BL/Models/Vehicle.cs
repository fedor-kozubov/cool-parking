﻿using System;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!Regex.IsMatch(id, @"[A-Z]{2}-\d{4}-[A-Z]{2}") || id.Length != 10)
            {
                throw new ArgumentException("Invalid Registration Plate Number", id);
            }
            if (balance <= 0m)
            {
                throw new ArgumentException("Negative or emopty  balance", $"{balance}");
            }
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }


        // adding a new random identifier to the storage to avoid duplication 
        private static Random rnd = new Random();
        private static List<string> generatedId = new List<string>();
        public static string GenerateRandomRegistrationPlateNumber()
        {
            // ID формата ХХ-YYYY-XX (где X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра, например DV-2345-KJ).
            // ASCII: 65..90 'A'..'Z', 48..57 '0'..'9'
            StringBuilder sb = new StringBuilder(10);
            sb.Append((char)rnd.Next(65, 91));
            sb.Append((char)rnd.Next(65, 91));
            sb.Append('-');
            sb.Append((char)rnd.Next(48, 58));
            sb.Append((char)rnd.Next(48, 58));
            sb.Append((char)rnd.Next(48, 58));
            sb.Append((char)rnd.Next(48, 58));
            sb.Append('-');
            sb.Append((char)rnd.Next(65, 91));
            sb.Append((char)rnd.Next(65, 91));
            string newRandomId = sb.ToString();

            if (generatedId.Contains(newRandomId))
            {
                return GenerateRandomRegistrationPlateNumber();
            }
            else
            {
                generatedId.Add(newRandomId);
                return newRandomId;
            }
        }

        public override string ToString()
        {
            return $"{Id}\t{VehicleType}\t{Balance}";
        }

    }
}