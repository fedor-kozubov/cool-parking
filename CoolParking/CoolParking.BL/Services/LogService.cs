﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }
        private static object locker = new object();

        public LogService(string logFilePath)
        {
            LogPath = logFilePath;
        }

        public void Write(string logInfo)
        {
            if (!String.IsNullOrEmpty(logInfo))
            {
                lock (locker)
                {
                    using (var file = new StreamWriter(LogPath, true))
                    {
                        file.WriteLine(logInfo);
                    }
                }
            }
        }

        public string Read()
        {
            StreamReader file = null;
            lock (locker)
            {
                try
                {
                    file = new StreamReader(LogPath);
                    return file.ReadToEnd();
                }
                catch (FileNotFoundException ex)
                {
                    throw new InvalidOperationException("File not found", ex);
                }
                finally
                {
                    file?.Close();
                }
            }
        }
    }
}