﻿using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer = new Timer();
        public event ElapsedEventHandler Elapsed;

        public double Interval { get; set; }

        public void Start()
        {
            timer.Interval = Interval;
            timer.Elapsed += Elapsed;
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }

        public void Dispose()
        {
            timer.Stop();
            timer = null;
        }
    }
}