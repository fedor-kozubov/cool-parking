﻿using System;
using System.Timers;
using System.Text;
using System.Collections.Generic;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking parking;
        private ITimerService withdrawTimer;
        private ITimerService logTimer;
        private ILogService logService;
        private List<TransactionInfo> transactions = null;
        private static object locker = new object();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            parking = Parking.GetInstance();
            transactions = new List<TransactionInfo>();
            this.logService = logService;

            this.withdrawTimer = withdrawTimer;
            this.withdrawTimer.Elapsed += OnWithdrawTimerEvent;
            this.withdrawTimer.Start();

            this.logTimer = logTimer;
            this.logTimer.Elapsed += OnLogTimerEvent;
            this.logTimer.Start();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public decimal GetLastProfit()
        {
            decimal sum = 0m;
            lock (locker)
            {
                foreach (TransactionInfo transactionInfo in transactions)
                {
                    sum += transactionInfo.Sum;
                }
            }
            return sum;
        }

        public int GetCapacity()
        {
            return parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return parking.Capacity - parking.Vehicles.Count;
        }

        public int GetHiredPlaces()
        {
            return parking.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0)
                throw new InvalidOperationException("No free places!");

            foreach (Vehicle _vehicle in parking.Vehicles)
            {
                if (_vehicle.Id == vehicle.Id)
                    throw new ArgumentException("Already existing vehicle Id");
            }
            lock (locker)
            {
                parking.Vehicles.Add(vehicle);
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            for (int i = 0; i < parking.Vehicles.Count; i++)
            {
                if (parking.Vehicles[i].Id == vehicleId)
                {
                    if (parking.Vehicles[i].Balance < 0m)
                        throw new InvalidOperationException("Attempt to remove vehicle with negative balance");

                    lock (locker)
                    {
                        parking.Vehicles.RemoveAt(i);
                    }
                    return;
                }
            }
            throw new ArgumentException("Attempt to remove unexisting vehicle");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0m)
                throw new ArgumentException("Negative sum");

            foreach (Vehicle vehicle in parking.Vehicles)
            {
                if (vehicle.Id == vehicleId)
                {
                    vehicle.Balance += sum;
                    return;
                }
            }
            throw new ArgumentException("Unexisting vehicle");
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            TransactionInfo[] result = null;
            lock (locker)
            {
                result = transactions.ToArray();
            }
            return result;
        }

        public string ReadFromLog()
        {
            try
            {
                return logService.Read();
            }
            catch (InvalidOperationException)
            {
                return "File not found!";
            }
            catch (Exception)
            {
                throw;
            }
        }



        private void OnWithdrawTimerEvent(Object source, ElapsedEventArgs e)
        {
            lock (locker)
            {
                foreach (Vehicle vehicle in parking.Vehicles)
                {
                    decimal tariff = Settings.Tariffs[(int)vehicle.VehicleType];

                    decimal sum;
                    if (vehicle.Balance >= tariff)
                        sum = tariff;
                    else if (vehicle.Balance >= 0m)
                        sum = (tariff - vehicle.Balance) * Settings.FineKoef + vehicle.Balance;
                    else
                        sum = tariff * Settings.FineKoef;

                    vehicle.Balance -= sum;
                    parking.Balance += sum;

                    transactions.Add(new TransactionInfo(vehicle.Id, sum));
                }
            }
        }

        private void OnLogTimerEvent(Object source, ElapsedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            lock (locker)
            {
                foreach (TransactionInfo transactionInfo in transactions)
                {
                    sb.Append(transactionInfo.ToString());
                    sb.Append("\r\n");
                }
                transactions.Clear();
            }
            logService.Write(sb.ToString());
        }



        public void Dispose()
        {
            withdrawTimer?.Dispose();
            logTimer?.Dispose();
            parking.Clear();
        }
    }
}

