﻿using System;
using System.IO;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using System.Reflection;
using System.Text;

namespace CoolParking.BL.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {

            string logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

            ITimerService withdrawTimer = new TimerService();
            withdrawTimer.Interval = Settings.WithdrawInterval * 1000;

            ITimerService logTimer = new TimerService();
            logTimer.Interval = Settings.LogInterval * 1000;

            ILogService logService = new LogService(logFilePath);
            ParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);

            /*
            Вывести на экран текущий баланс Парковки.
            Вывести на экран сумму заработанных денег за текущий период(до записи в лог).
            Вывести на экран количество свободных / занятых мест на парковке.
            Вывести на экран все Транзакции Парковки за текущий период(до записи в лог).
            Вывести историю транзакций(считав данные из файла Transactions.log).
            Вывести на экран список Тр.средств находящихся на Паркинге.
            Поставить Тр. средство на Паркинг.
            Забрать транспортное средство с Паркинга.
            Пополнить баланс конкретного Тр. средства.
            */

            const string CommandsList = "balance\r\n" +
                                        "lastprofit\r\n" +
                                        "places\r\n" +
                                        "lasttransactions\r\n" +
                                        "readlog\r\n" +
                                        "vehicles\r\n" +
                                        "addvehicle id sum type[passengercar | truck | bus | motorcycle]\r\n" +
                                        "removevehicle id\r\n" +
                                        "topupvehicle id sum\r\n" +
                                        "help\r\n" +
                                        "exit";

            Console.WriteLine("Welcome to CoolParking");
            Console.WriteLine("commands list:");
            Console.WriteLine(CommandsList);


            while (true)
            {
                // syntax: command id sum type
                string[] input = Console.ReadLine().Split(new char[] { ' ' });
                if (input.Length == 0) continue;

                string command = input[0].ToLower();

                string? id = (input.Length > 1) ? input[1] : null;

                decimal? sum = null;
                if (input.Length > 2)
                {
                    if (Decimal.TryParse(input[2], out decimal sum2))
                        sum = sum2;
                }

                VehicleType? vehicleType = null;
                if (input.Length > 3)
                {
                    switch (input[3].ToLower())
                    {
                        case "passengercar":
                            vehicleType = VehicleType.PassengerCar;
                            break;
                        case "truck":
                            vehicleType = VehicleType.Truck;
                            break;
                        case "bus":
                            vehicleType = VehicleType.Bus;
                            break;
                        case "motorcycle":
                            vehicleType = VehicleType.Motorcycle;
                            break;
                        default:
                            break;
                    }
                }


                switch (command)
                {
                    case "balance":
                        Console.WriteLine(parkingService.GetBalance());
                        break;

                    case "lastprofit":
                        Console.WriteLine(parkingService.GetLastProfit());
                        break;

                    case "places":
                        Console.WriteLine($"free: {parkingService.GetFreePlaces()}   hired: {parkingService.GetHiredPlaces()}");
                        break;

                    case "lasttransactions":
                        var transactions = parkingService.GetLastParkingTransactions();
                        StringBuilder sb = new StringBuilder();
                        foreach (TransactionInfo transactionInfo in transactions)
                        {
                            sb.Append(transactionInfo.ToString());
                            sb.Append("\r\n");
                        }
                        Console.WriteLine(sb.ToString());
                        break;

                    case "readlog":
                        Console.WriteLine($"{parkingService.ReadFromLog()}");
                        break;

                    case "vehicles":
                        var vehicles = parkingService.GetVehicles();
                        foreach (var vehicle in vehicles)
                            Console.WriteLine($"{vehicle.Id}");
                        break;

                    case "addvehicle":
                        if (id == null || sum == null || vehicleType == null)
                        {
                            Console.WriteLine("Wrong format");
                            break;
                        }
                        try
                        {
                            parkingService.AddVehicle(new Vehicle(id, (VehicleType)vehicleType, (Decimal)sum));
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine($"Error {ex.Message}");
                        }
                        break;

                    case "removevehicle":
                        if (id == null)
                        {
                            Console.WriteLine("Wrong format");
                            break;
                        }
                        try
                        {
                            parkingService.RemoveVehicle(id);
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine($"Error {ex.Message}");
                        }
                        break;

                    case "topupvehicle":
                        if (id == null || sum == null)
                        {
                            Console.WriteLine("Wrong format");
                            break;
                        }
                        try
                        {
                            parkingService.TopUpVehicle(id, (decimal)sum);
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine($"Error {ex.Message}");
                        }
                        break;

                    case "help":
                        Console.WriteLine(CommandsList);
                        break;

                    case "":
                        break;

                    case "exit":
                        return;

                    default:
                        Console.WriteLine($"Unknown command {input[0]}");
                        break;
                }
            }
        }
    }
}
